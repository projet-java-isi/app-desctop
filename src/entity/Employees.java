package entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Employees {
    private int id;
    private String nom;
    private String prenom;
    private String cnss;
    private String tel;
    private String adresse;
    private String image;
    private Date dateEmbauche;
    private Date dateNais;
    private String fonction;
    private int idsociete;
    private String email;
    private String password;
    private String cin;
    private String genre;
    private String salaire;
    private String rib;
    private int idSite;
    private Collection<Demandes> demandesById;
    private Societes societesByIdsociete;
    private Sites sitesByIdSite;
    private Collection<Presences> presencesById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom", nullable = true, length = 100)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom", nullable = true, length = 100)
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "cnss", nullable = true, length = 100)
    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    @Basic
    @Column(name = "tel", nullable = true, length = 100)
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Basic
    @Column(name = "adresse", nullable = true, length = 100)
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "image", nullable = true, length = 100)
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "dateEmbauche", nullable = true)
    public Date getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(Date dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    @Basic
    @Column(name = "dateNais", nullable = true)
    public Date getDateNais() {
        return dateNais;
    }

    public void setDateNais(Date dateNais) {
        this.dateNais = dateNais;
    }

    @Basic
    @Column(name = "fonction", nullable = true, length = 100)
    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    @Basic
    @Column(name = "idsociete", nullable = false)
    public int getIdsociete() {
        return idsociete;
    }

    public void setIdsociete(int idsociete) {
        this.idsociete = idsociete;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password", nullable = true, length = 100)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "cin", nullable = true, length = 100)
    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    @Basic
    @Column(name = "genre", nullable = true, length = 100)
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Basic
    @Column(name = "salaire", nullable = true, length = 100)
    public String getSalaire() {
        return salaire;
    }

    public void setSalaire(String salaire) {
        this.salaire = salaire;
    }

    @Basic
    @Column(name = "rib", nullable = true, length = 100)
    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    @Basic
    @Column(name = "idSite", nullable = false)
    public int getIdSite() {
        return idSite;
    }

    public void setIdSite(int idSite) {
        this.idSite = idSite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employees employees = (Employees) o;
        return id == employees.id && idsociete == employees.idsociete && idSite == employees.idSite && Objects.equals(nom, employees.nom) && Objects.equals(prenom, employees.prenom) && Objects.equals(cnss, employees.cnss) && Objects.equals(tel, employees.tel) && Objects.equals(adresse, employees.adresse) && Objects.equals(image, employees.image) && Objects.equals(dateEmbauche, employees.dateEmbauche) && Objects.equals(dateNais, employees.dateNais) && Objects.equals(fonction, employees.fonction) && Objects.equals(email, employees.email) && Objects.equals(password, employees.password) && Objects.equals(cin, employees.cin) && Objects.equals(genre, employees.genre) && Objects.equals(salaire, employees.salaire) && Objects.equals(rib, employees.rib);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, cnss, tel, adresse, image, dateEmbauche, dateNais, fonction, idsociete, email, password, cin, genre, salaire, rib, idSite);
    }

    @OneToMany(mappedBy = "employeesByIdEmployee")
    public Collection<Demandes> getDemandesById() {
        return demandesById;
    }

    public void setDemandesById(Collection<Demandes> demandesById) {
        this.demandesById = demandesById;
    }

    @ManyToOne
    @JoinColumn(name = "idsociete", referencedColumnName = "id", nullable = false)
    public Societes getSocietesByIdsociete() {
        return societesByIdsociete;
    }

    public void setSocietesByIdsociete(Societes societesByIdsociete) {
        this.societesByIdsociete = societesByIdsociete;
    }

    @ManyToOne
    @JoinColumn(name = "idSite", referencedColumnName = "idSite", nullable = false)
    public Sites getSitesByIdSite() {
        return sitesByIdSite;
    }

    public void setSitesByIdSite(Sites sitesByIdSite) {
        this.sitesByIdSite = sitesByIdSite;
    }

    @OneToMany(mappedBy = "employeesByIdEmployee")
    public Collection<Presences> getPresencesById() {
        return presencesById;
    }

    public void setPresencesById(Collection<Presences> presencesById) {
        this.presencesById = presencesById;
    }
}
