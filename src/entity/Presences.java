package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Presences {
    private int id;
    private String nbJours;
    private Integer mois;
    private Integer annee;
    private String salaire;
    private int idEmployee;
    private Integer confirm;
    private Employees employeesByIdEmployee;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nbJours", nullable = false, length = 100)
    public String getNbJours() {
        return nbJours;
    }

    public void setNbJours(String nbJours) {
        this.nbJours = nbJours;
    }

    @Basic
    @Column(name = "mois", nullable = true)
    public Integer getMois() {
        return mois;
    }

    public void setMois(Integer mois) {
        this.mois = mois;
    }

    @Basic
    @Column(name = "annee", nullable = true)
    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    @Basic
    @Column(name = "salaire", nullable = true, length = 100)
    public String getSalaire() {
        return salaire;
    }

    public void setSalaire(String salaire) {
        this.salaire = salaire;
    }

    @Basic
    @Column(name = "idEmployee", nullable = false)
    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    @Basic
    @Column(name = "confirm", nullable = true)
    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Presences presences = (Presences) o;
        return id == presences.id && idEmployee == presences.idEmployee && Objects.equals(nbJours, presences.nbJours) && Objects.equals(mois, presences.mois) && Objects.equals(annee, presences.annee) && Objects.equals(salaire, presences.salaire) && Objects.equals(confirm, presences.confirm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nbJours, mois, annee, salaire, idEmployee, confirm);
    }

    @ManyToOne
    @JoinColumn(name = "idEmployee", referencedColumnName = "id", nullable = false)
    public Employees getEmployeesByIdEmployee() {
        return employeesByIdEmployee;
    }

    public void setEmployeesByIdEmployee(Employees employeesByIdEmployee) {
        this.employeesByIdEmployee = employeesByIdEmployee;
    }
}
