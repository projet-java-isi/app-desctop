package entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Demandes {
    private int id;
    private String etat;
    private String etatRh;
    private String type;
    private Timestamp currentDate;
    private int idEmployee;
    private String file;
    private Timestamp userValidationDate;
    private Timestamp adminValidationDate;
    private Employees employeesByIdEmployee;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "etat", nullable = false, length = 100)
    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @Basic
    @Column(name = "etatRH", nullable = false, length = 100)
    public String getEtatRh() {
        return etatRh;
    }

    public void setEtatRh(String etatRh) {
        this.etatRh = etatRh;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 100)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "currentDate", nullable = false)
    public Timestamp getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Timestamp currentDate) {
        this.currentDate = currentDate;
    }

    @Basic
    @Column(name = "idEmployee", nullable = false)
    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    @Basic
    @Column(name = "file", nullable = true, length = 300)
    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Basic
    @Column(name = "userValidationDate", nullable = true)
    public Timestamp getUserValidationDate() {
        return userValidationDate;
    }

    public void setUserValidationDate(Timestamp userValidationDate) {
        this.userValidationDate = userValidationDate;
    }

    @Basic
    @Column(name = "adminValidationDate", nullable = true)
    public Timestamp getAdminValidationDate() {
        return adminValidationDate;
    }

    public void setAdminValidationDate(Timestamp adminValidationDate) {
        this.adminValidationDate = adminValidationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Demandes demandes = (Demandes) o;
        return id == demandes.id && idEmployee == demandes.idEmployee && Objects.equals(etat, demandes.etat) && Objects.equals(etatRh, demandes.etatRh) && Objects.equals(type, demandes.type) && Objects.equals(currentDate, demandes.currentDate) && Objects.equals(file, demandes.file) && Objects.equals(userValidationDate, demandes.userValidationDate) && Objects.equals(adminValidationDate, demandes.adminValidationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, etat, etatRh, type, currentDate, idEmployee, file, userValidationDate, adminValidationDate);
    }

    @ManyToOne
    @JoinColumn(name = "idEmployee", referencedColumnName = "id", nullable = false)
    public Employees getEmployeesByIdEmployee() {
        return employeesByIdEmployee;
    }

    public void setEmployeesByIdEmployee(Employees employeesByIdEmployee) {
        this.employeesByIdEmployee = employeesByIdEmployee;
    }
}
