package entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Societes {
    private int id;
    private String nom;
    private String rc;
    private String logo;
    private String adresse;
    private String tel;
    private String email;
    private String capital;
    private String cnss;
    private String ncompte;
    private String patante;
    private String identifiantFiscal;
    private String stamp;
    private String icon;
    private Collection<Employees> employeesById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom", nullable = true, length = 100)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "rc", nullable = true, length = 200)
    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    @Basic
    @Column(name = "logo", nullable = true, length = 100)
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Basic
    @Column(name = "adresse", nullable = true, length = 100)
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "tel", nullable = true, length = 100)
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "capital", nullable = true, length = 100)
    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Basic
    @Column(name = "cnss", nullable = true, length = 100)
    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    @Basic
    @Column(name = "ncompte", nullable = true, length = 500)
    public String getNcompte() {
        return ncompte;
    }

    public void setNcompte(String ncompte) {
        this.ncompte = ncompte;
    }

    @Basic
    @Column(name = "patante", nullable = true, length = 100)
    public String getPatante() {
        return patante;
    }

    public void setPatante(String patante) {
        this.patante = patante;
    }

    @Basic
    @Column(name = "identifiantFiscal", nullable = true, length = 100)
    public String getIdentifiantFiscal() {
        return identifiantFiscal;
    }

    public void setIdentifiantFiscal(String identifiantFiscal) {
        this.identifiantFiscal = identifiantFiscal;
    }

    @Basic
    @Column(name = "stamp", nullable = true, length = 100)
    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    @Basic
    @Column(name = "icon", nullable = true, length = 100)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Societes societes = (Societes) o;
        return id == societes.id && Objects.equals(nom, societes.nom) && Objects.equals(rc, societes.rc) && Objects.equals(logo, societes.logo) && Objects.equals(adresse, societes.adresse) && Objects.equals(tel, societes.tel) && Objects.equals(email, societes.email) && Objects.equals(capital, societes.capital) && Objects.equals(cnss, societes.cnss) && Objects.equals(ncompte, societes.ncompte) && Objects.equals(patante, societes.patante) && Objects.equals(identifiantFiscal, societes.identifiantFiscal) && Objects.equals(stamp, societes.stamp) && Objects.equals(icon, societes.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, rc, logo, adresse, tel, email, capital, cnss, ncompte, patante, identifiantFiscal, stamp, icon);
    }

    @OneToMany(mappedBy = "societesByIdsociete")
    public Collection<Employees> getEmployeesById() {
        return employeesById;
    }

    public void setEmployeesById(Collection<Employees> employeesById) {
        this.employeesById = employeesById;
    }
}
