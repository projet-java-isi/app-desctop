package entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Sites {
    private int idSite;
    private String name;
    private Collection<Employees> employeesByIdSite;

    @Id
    @Column(name = "idSite", nullable = false)
    public int getIdSite() {
        return idSite;
    }

    public void setIdSite(int idSite) {
        this.idSite = idSite;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sites sites = (Sites) o;
        return idSite == sites.idSite && Objects.equals(name, sites.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSite, name);
    }

    @OneToMany(mappedBy = "sitesByIdSite")
    public Collection<Employees> getEmployeesByIdSite() {
        return employeesByIdSite;
    }

    public void setEmployeesByIdSite(Collection<Employees> employeesByIdSite) {
        this.employeesByIdSite = employeesByIdSite;
    }
}
